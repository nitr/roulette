<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;


    /* @var $this yii\web\View */

    $this->title = 'K50 test task';
?>
<div class="site-index">

    <div class="body-content">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'fieldsCount') ?>

        <?= $form->field($model, 'chipCount') ?>

        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
