<?php
namespace app\components;

/**
 * Class Roulette
 */
class Roulette {

    /**
     * @var int
     */
    private $fieldsCount;
    /**
     * @var int
     */
    private $chipCount;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @var int
     */
    private $min = 10;

    /**
     * Roulette constructor.
     * @param integer $fieldsCount
     * @param integer $chipCount
     */
    public function __construct($fieldsCount, $chipCount)
    {
        $this->fieldsCount = $fieldsCount;
        $this->chipCount = $chipCount;
    }

    /**
     * Set path to file
     * @param string $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Set min value
     * @param int $min
     */
    public function setMin($min)
    {
        $this->min = $min;
    }

    /**
     * Generate all combinations and save result to file
     * @throws Exception
     */
    public function generate()
    {
        if ($this->chipCount > $this->fieldsCount) {
            throw new Exception('Chip Count must be less or equal than fields count.');
        }

        if (!is_file($this->filePath) || !is_writable($this->filePath)) {
            throw new Exception('Wrong file.');
        }

        $return = [];

        $min = implode("", array_merge(array_fill(0, $this->fieldsCount - $this->chipCount, 0), array_fill(0, $this->chipCount, 1)));

        if ($this->chipCount != $this->fieldsCount) {
            $max = strrev($min);

            $min = bindec($min);
            $max = bindec($max);


            for ($i = $min; $i <= $max; $i++) {
                $value = (string)decbin($i);
                if ($this->chipCount == array_sum(str_split($value))) {
                    $value = str_pad($value, $this->fieldsCount, '0', STR_PAD_LEFT);
                    $return[$value] = $value;
                }
            }
        } else {
            $return[$min] = $min;
        }

        $count = count($return);

        if ($count < $this->min) {
            $content = 'менее ' . $this->min . ' вариантов';
        } else {
            $content = $count . "\r\n" . implode("\r\n", $return);
        }

        file_put_contents($this->filePath, $content);
    }

    /**
     * Generate all combinations and save result to file
     * @throws Exception
     */
    public function generateRecursive()
    {
        if ($this->chipCount > $this->fieldsCount) {
            throw new Exception('Chip Count must be less or equal than fields count.');
        }

        if (!is_file($this->filePath) || !is_writable($this->filePath)) {
            throw new Exception('Wrong file.');
        }

        $return = [];

        $data = array_merge(array_fill(0, $this->chipCount, 1), array_fill(0, $this->chipCount - $this->chipCount, 0));

        foreach ($this->permutation($data) as $permutation) {
            $value = implode('', $permutation);
            $return[$value] = $value;
        }

        $count = count($return);

        if ($count < $this->min) {
            $content = 'менее ' . $this->min . ' вариантов';
        } else {
            $content = $count . "\r\n" . implode("\r\n", $return);
        }

        file_put_contents($this->filePath, $content);
    }

    /**
     * Generator all array combinations
     * @param array $combinations
     * @return Generator
     */
    private function permutation(array $combinations)
    {
        if (count($combinations) <= 1) {
            yield $combinations;
        } else {
            foreach ($this->permutation(array_slice($combinations, 1)) as $permutation) {
                foreach (range(0, count($combinations) - 1) as $i) {
                    yield array_merge(
                        array_slice($permutation, 0, $i),
                        [$combinations[0]],
                        array_slice($permutation, $i)
                    );
                }
            }
        }
    }
}