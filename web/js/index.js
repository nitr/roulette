$(function() {
    $('#conversion_period').change(function() {
        window.location.href = window.location.pathname + '?period=' + $(this).val();
    });
});