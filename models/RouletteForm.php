<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\Roulette;

class RouletteForm extends Model
{
    const MIN_COMBINATIONS = 10;

    public $fieldsCount;
    public $chipCount;

    public function rules()
    {
        return [
            [['fieldsCount', 'chipCount'], 'required'],
            [['fieldsCount', 'chipCount'], 'integer'],
            ['fieldsCount', 'compare', 'compareAttribute' => 'chipCount', 'operator' => '>=', 'type' => 'number'],
        ];
    }

    public function combine()
    {
        if (!$this->validate()) {
            return false;
        }

        $directory = Yii::getAlias('@app/runtime');
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }
        $filePath = $directory . DIRECTORY_SEPARATOR . uniqid(time(), true);
        file_put_contents($filePath, "");

        // Указываем количество ячеек и количество монет
        $roulette = new Roulette($this->fieldsCount, $this->chipCount);

        // Указываем минимальное значение
        $roulette->setMin(static::MIN_COMBINATIONS);

        // Указываем путь к файлю с результатами
        $roulette->setFilePath($filePath);

        // Самый оптимальный по моему мнению
        $roulette->generate();

        // Один из способов с использованием рекурсии генератора
        //$roulette->generateRecursive();
    }

    public function attributeLabels() {

        return  [
            'fieldsCount' => 'Количество ячеек',
            'chipCount' => 'Количество фишек'
        ];
    }
}